package com.foo.time;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TimeServiceTest {

	@Test
	public void testTimeRequestLogged() {
		TimeService service = new TimeServiceImpl();
		String time = service.getTime().getTime();
		assertEquals(time, service.getTimeLog().getTimesList().get(0).getTime());
	}

	@Test
	public void testLogSize() {
		TimeService service = new TimeServiceImpl();

		for (int i = 0; i < TimeServiceImpl.LOG_CAPACITY * 2; i++) {
			service.getTime();
		}

		assertEquals(TimeServiceImpl.LOG_CAPACITY, service.getTimeLog().getTimesCount());
	}

}
