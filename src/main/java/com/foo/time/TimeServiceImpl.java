package com.foo.time;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Queue;

import org.springframework.stereotype.Service;

import com.foo.time.TimeProtos.Time;
import com.foo.time.TimeProtos.Times;
import com.foo.time.TimeProtos.Times.Builder;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;

@Service
public class TimeServiceImpl implements TimeService {

	public static final int LOG_CAPACITY = 10;

	private Queue<Time> timeQueue = Queues.synchronizedQueue(EvictingQueue.create(LOG_CAPACITY));

	@Override
	public Time getTime() {
		Time time = Time.newBuilder().setTime(ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME))
				.build();
		timeQueue.add(time);
		return time;
	}

	@Override
	public Times getTimeLog() {
		Builder timesBuilder = Times.newBuilder();
		timesBuilder.addAllTimes(timeQueue);
		return timesBuilder.build();
	}

}
