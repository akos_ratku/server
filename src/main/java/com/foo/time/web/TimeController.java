package com.foo.time.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foo.time.TimeProtos.Time;
import com.foo.time.TimeProtos.Times;
import com.foo.time.TimeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired) )
public class TimeController {

	private final TimeService timeService;

	@RequestMapping("/time")
	public Time getTime() {
		return timeService.getTime();
	}

	@RequestMapping("/time/log")
	public Times getTimeLog() {
		return timeService.getTimeLog();
	}

}
