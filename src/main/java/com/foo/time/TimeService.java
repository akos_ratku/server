package com.foo.time;

import com.foo.time.TimeProtos.Time;
import com.foo.time.TimeProtos.Times;

public interface TimeService {

	Time getTime();

	Times getTimeLog();

}
